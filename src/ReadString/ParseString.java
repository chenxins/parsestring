package ReadString;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class ParseString 
{

	public static void main(String args[]) throws IOException
	{

		ReadFile read = new ReadFile();
		ArrayList<String> info = new ArrayList<String>();
		HashMap<String, String> exactMatched = new HashMap<String, String>();
		
		//read the input file by lines, store the information in an ArrayList info
		info = read.readFileByLines("titles.txt");
			
		//check every string in the info
		for (int t = 0; t < info.size(); t++) 
		{

				if (!info.get(t).isEmpty() && isNumeric(info.get(t).substring(0)) && info.get(t).length() < 10)
				{
					exactMatched.put(info.get(t), "true");
					info.remove(t);
				}
				else if(containsA(info.get(t)))
				{
					exactMatched.put(info.get(t), "this string contains a");
					info.remove(t);
				}
				else
				{
					continue;
				}
			
		}

		
		// print the result
		PrintWriter writer = new PrintWriter("result.txt", "UTF-8");


		Iterator<Entry<String, String>> iter2 = exactMatched.entrySet().iterator();
		while (iter2.hasNext())
		{
			Map.Entry entry = (Map.Entry) iter2.next();
			Object key = entry.getKey();
			if (entry.getValue() == "true") 
			{
				writer.println(entry.getKey() + "  is valid " );

				System.out.println(entry.getKey() + "  is valid " );
			}

		}
		writer.close();

	}
	//check if the string is number
	public static boolean isNumeric(String str)
	{
	    for (char c : str.toCharArray())
	    {
	        if (!Character.isDigit(c)) return false;
	    }
	    return true;
	}
	
	//check if the sting contains a
	public static boolean containsA(String str)
	{
		if(str.contains("a"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
}

